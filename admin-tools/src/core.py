import asyncore
import socket
import signal
import logging
import json
import threading
import time
import os
import actions
import api.ctl
import api.remote
import api.config


class Core:

    TIMER_SECONDS = 300

    def __init__(self):
        self.connections = [api.ctl.Connection(
            ctl, self) for ctl in api.config.file.ctls]
        self.need_exit = False
        self.timer_thread = threading.Thread(target=self.periodic)
        self.timer_thread.start()
        signal.signal(signal.SIGINT, self.exit_signal)
        signal.signal(signal.SIGTERM, self.exit_signal)
        # init agents list
        self.init_agent_list()
        self.alisteners = []

        for listener in api.config.file.listen:
            try:
                new_listener = api.remote.AgentListener(listener, self)
                self.alisteners += [new_listener]
            except socket.error as err:
                logging.error("Error init listener: %s", err.strerror)
            except Exception as err:
                logging.error("Exception: " + str(err))


    def periodic(self):
        file = api.config.file.ctls[0].file
        logging.basicConfig(
            filename=api.config.file.logfile, level=logging.DEBUG)
        logging.debug("run periodic task: '%s'", file)
        while not self.need_exit:
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            sock.connect(file)
            sock.send('{"func":"timer"}')
            sock.close()
            for i in range(1, Core.TIMER_SECONDS):
                time.sleep(1)
                if self.need_exit:
                    break

    def exit_signal(self, sig, frame):
        self.exit()

    def init_agent_list(self):
        self.agents = api.remote.AgentCollection()
        for agent in api.config.file.agents:

            self.agents.add_agent(api.remote.make_agent(agent.host,
                                                        agent.port,
                                                        self,
                                                        agent.proto,
                                                        ssl_cert=agent.ssl_cert,
                                                        ssl_key=agent.ssl_key,
                                                        ssl_ca=agent.ssl_ca,
                                                        atype='passive'))

    def execute(self, session):
        client = session.connection
        try:

            logging.info('executing "%s" for %s',
                         session.func, session.file)

            action = actions.collection[session.func]
            action.prepare_out(session)
            action.execute(self, session)

        except api.nxerrors.ActionError as err:
            logging.error('error %d ("%s")', err.num, err.reason)
            session.pick_error(err.num, err.reason)
        except Exception as err:
            logging.error('common error %s', err)
            session.pick_error(api.nxerrors.ActionError.SYSTEM, str(err))


        outbuf = json.dumps(session.out)
        client.send(outbuf)

    def call(self, func, **params):
        session = api.action.Session("internal")
        session.add_params(**params)
        try:
            logging.info('call "%s", params: %s', func, params)
            action = actions.collection[func]
            action.prepare_out(session)
            action.execute(self, session)
        except api.nxerrors.ActionError as err:
            logging.error('error %d ("%s")', err.num, err.reason)
            session.pick_error(err.num, err.reason)
        return session.out

    def exit(self):
        logging.info('exiting... ')
        for connection in self.connections:
            if os.path.exists(connection.file):
                os.unlink(connection.file)
        self.need_exit = True
        logging.debug("join...")
        self.timer_thread.join()
        logging.debug("done")
        asyncore.close_all()
        for connection in self.connections:
            for client in connection.clients:
                client.close()
            connection.close()
        for listener in self.alisteners:
            listener.close()


def start():
    core = Core()
    while not core.need_exit:
        asyncore.loop(count=1)
    core.exit()

if __name__ == '__main__':
    api.config.reload_init(None)
    logging.basicConfig(filename=api.config.file.logfile, level=logging.DEBUG)
    start()
