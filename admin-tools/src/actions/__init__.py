import sys
import os
import re
from os.path import join, basename
import inspect
import importlib
import api


class ActionCollection:
    actions = {}

    def __init__(self):
        actions_dir = join(os.getcwd(), 'actions')
        files = os.listdir(actions_dir)
        for name in files:
            if name == '__init__.py' or not name.endswith('.py'):
                continue
            res = re.match("(.*).py$", name)
            modulename = "actions." + res.group(1)
            module = importlib.import_module(modulename)
            symbols = dir(module)
            for symbol in symbols:
                if symbol == 'Action':
                    # Skip base action class
                    continue
                symbol_obj = getattr(module, symbol)
                if inspect.isclass(symbol_obj):
                    if issubclass(symbol_obj, api.action.Action):
                        #logging.info("registering action '%s'", symbol)
                        obj = symbol_obj()
                        obj.actions = self
                        self.actions[obj.name] = obj

    def __len__(self):
        return len(self.actions)

    def __getitem__(self, name):
        if name not in self.actions:
            raise api.nxerrors.NotDefined('action', name)
        return self.actions[name]

    def __contains__(self, item):
        return item in self.actions

    def __iter__(self):
        return iter(self.actions)


"""
Loading modules with actions
"""

collection = ActionCollection()
