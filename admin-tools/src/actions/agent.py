import logging
import json
import os.path
import api
import datetime

from api.action import Action


class ActionAgentList(Action):

    def __init__(self):
        Action.__init__(self, "agent", 'get server list')

    def execute(self, core, session):
        for agent in core.agents.agents:
            session.add_element(
                tag='agents',
                elid=agent.id,
                host=agent.ip,
                port=agent.port,
                status=str(agent.status.name),
                type=agent.proto,
                description=agent.agent_type
            )


class ActionAgentCheck(Action):

    def __init__(self):
        Action.__init__(self, 'agent.check',
                        'check agent connection and receive server info')

    def execute(self, core, session):
        for agent in core.agents.agents:
            try:
                logging.info('checking agent %d: %s', agent.id, agent.ip)
                result = agent.request('serverInfo')
                info = result['data']['server-info']
                agent.updatestatus(
                    api.remote.AgentStatus.ONLINE, json.dumps(info))
                session.add_params(status="ONLINE")

            except:
                logging.error('... failed')
                session.add_params(status="OFFLINE")

"""

  Get info: module info/server info

"""


class ActionServerInfo(Action):

    def __init__(self):
        Action.__init__(self, 'agent.info', 'get server info')

    def execute(self, core, session):
        if not "id" in session.json:
            raise api.nxerrors.Missed("field", "id")

        agent_id = session.json["id"]
        logging.debug("get server info abount agent id %s", agent_id)
        logging.debug("agent count: %d", len(core.agents))
        print(core.agents)

        server = core.agents.get(agent_id)
        result = server.request('serverInfo')
        info = result['data']['server-info']
        server.updatestatus(api.remote.AgentStatus.ONLINE, json.dumps(info))
        modules = ", ".join([key for key in info['modules'].keys()])

        # convert
        started_h = datetime.datetime.fromtimestamp(
            int(info['started']) / 1000000
        )
        servertime_h = datetime.datetime.fromtimestamp(
            int(info['servertime']) / 1000000
        )

        session.add_params(load=info['load'], started=info['started'],
                           started_h=started_h.isoformat(" "),
                           hostname=info['hostname'], mem=info['mem'],
                           pid=info['pid'], servertime=info['servertime'],
                           servertime_h=servertime_h.isoformat(' '),
                           version=info['version'],
                           systeminfo=info['systeminfo'], os=info['os'],
                           modules=modules)


class ActionModuleInfo(Action):

    def __init__(self):
        Action.__init__(self, 'module.info',
                        '<id=server-ID name=MODULE NAME> get module info ')

    def execute(self, core, session):
        server = self.get_agent(core, session)
        name = session.name

        result = server.request('moduleInfo', name=name)
        info = dict(result['data'][name])
        logging.debug('result: %s', str(info))
        session.add_dict(info)
        logging.debug('result: %s', json.dumps(result, indent=4))


"""

  Files: put/get

"""


class ActionGetFile(Action):

    def __init__(self):
        Action.__init__(self, 'agent.file.get',
                        'get file from agent ACL. Params: id, filename, filetype, content, target')

    def execute(self, core, session):
        server = self.get_agent(core, session)
        filename = session.filename
        filetype = session.filetype
        cwd = session.cwd
        result = server.request('getFile',
                                filetype=filetype,
                                filename=filename)
        content = result['data']['file']
        file_content = content

        target = os.path.normpath(session.target)
        if target[0] != os.path.sep:
            # relative path
            target = os.path.join(cwd, target)
        with open(os.path.join(cwd, target), 'w') as outfile:
            outfile.write(file_content)


class ActionPutFile(Action):

    def __init__(self):
        Action.__init__(self, 'agent.file.put',
                        'put file to agent ACL. Params: id, filename, filetype, source, target')

    def execute(self, core, session):
        server = self.get_agent(core, session)
        filename = session.filename
        filetype = session.filetype

        source = os.path.normpath(session.source)
        if not source.startswith(os.path.sep):
            source = os.path.join(session.cwd, source)

        with open(source, 'r') as file:
            data = file.read()
            server.request('putFile', filetype=filetype,
                           filename=filename,
                           file=data)

"""

  Module start/stop/restart

"""


class ActionModuleRestart(Action):

    def __init__(self):
        Action.__init__(
            self,
            "module.restart",
            "Restart module on remote agent. Params: id (server id), name (module name) ")

    def execute(self, core, session):
        self.get_agent(core, session).request(
            'moduleRestart', name=session.name)


class ActionModuleStart(Action):

    def __init__(self):
        Action.__init__(
            self,
            "module.start",
            "Start module on remote agent. Params: id (server id), name (module name) ")

    def execute(self, core, session):
        self.get_agent(core, session).request(
            'moduleStart', name=session.name)


class ActionModuleStop(Action):

    def __init__(self):
        Action.__init__(
            self,
            "module.stop",
            "Stop module on remote agent. Params: id (server id), name (module name) ")

    def execute(self, core, session):
        self.get_agent(core, session).request(
            'moduleStop', name=session.name)

"""

  Server Start/Stop/Restart

"""


class ActionServerRestart(Action):

    def __init__(self):
        Action.__init__(
            self,
            "agent.restart",
            "Restart remote agent. Params: id (server id)")

    def execute(self, core, session):
        self.get_agent(core, session).request('serverRestart')


class ActionServerStart(Action):

    def __init__(self):
        Action.__init__(
            self,
            "agent.start",
            "Start remote agent. Params: id (server id)")

    def execute(self, core, session):
        self.get_agent(core, session).request('serverStart')


class ActionServerStop(Action):

    def __init__(self):
        Action.__init__(
            self,
            "agent.stop",
            "Stop remote agent. Params: id (server id)")

    def execute(self, core, session):
        self.get_agent(core, session).request('serverStop')

"""

  Routes

"""


class ActionRouteRestart(Action):

    def __init__(self):
        Action.__init__(
            self,
            "route.restart",
            "Restart route on remote agent. Params: id (server id)")

    def execute(self, core, session):
        raise api.nxerrors.NotImplemented(self.name)


class ActionRouteStart(Action):

    def __init__(self):
        Action.__init__(
            self,
            "route.start",
            "Start route on remote agent. Params: id (server id)")

    def execute(self, core, session):
        raise api.nxerrors.NotImplemented(self.name)


class ActionRouteStop(Action):

    def __init__(self):
        Action.__init__(
            self,
            "route.stop",
            "Stop route on remote agent. Params: id (server id)")

    def execute(self, core, session):
        raise api.nxerrors.NotImplemented(self.name)
