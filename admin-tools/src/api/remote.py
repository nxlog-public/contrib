import os
from errno import ENOENT

import socket
import ssl
import json
import logging
import email
import asyncore
import api.borg
import api.nxerrors
import time
from enum import Enum


class AgentStatus(Enum):
    OFFLINE = 0
    ONLINE = 1


class AgentReply:
    """
    Parse reply from remote Agent.
    AgentReply can determine end-of-transmission
    There are two stages:

    1) accepts some bytes and try to distinguish http headers
    2) after that, AgentReply parse 'Content-Length' header
    3) accepts block of bytes until content-lengs was reached

    """

    def __init__(self):
        self.error = ''
        self.code = ''
        self.httpVer = ''
        self.content_type = ''
        self.content = ''
        self.input_buf = ''
        self.content_length = 100000
        self.headers = ''
        self.buf = ''
        self.has_content_length = False
        self.header_filled = False

    def add_chars(self, buf):
        """ add received chars to buffer and try to parse them """
        logging.debug("readen chunk of %d bytes", len(buf))
        self.buf += buf
        if self.header_filled:
            self.content += str(buf)
        else:
            self.input_buf += str(buf)
            if self.input_buf.find('\r\n\r\n') >= 0:
                self.headers, self.content = self.input_buf.split(
                    '\r\n\r\n', 1)
                self.header_filled = True
                self.parse_head()
                if self.code != '200':
                    return False
        logging.debug('received %d bytes, wait: %d',
                      len(self.content), self.content_length - len(self.content))
        return len(self.content) >= self.content_length

    def parse_head(self):
        """
        parse http headers
        """
        if self.headers.find('\r\n') == -1:
            # first line only
            request_parts = self.headers.split(' ')
            self.httpVer = request_parts[0]
            self.code = request_parts[1]
            self.error = ' '.join(request_parts[2:])
            self.has_content_length = False
            return

        first, other = self.headers.split('\r\n', 1)
        request_parts = first.split(' ')
        self.httpVer = request_parts[0]
        self.code = request_parts[1]
        self.error = request_parts[2]
        message = email.message_from_string(other)
        headers = dict(message.items())
        if 'Content-Type' in headers:
            self.content_type = headers['Content-Type']
        if 'Content-Length' in headers:
            self.content_length = int(headers['Content-Length'])
            self.has_content_length = True


class AgentId(api.borg.Borg):
    """
    Singleton class to hold agent id (autoincrement)
    """
    next_id = 0

    def get_next(self):
        self.next_id += 1
        return self.next_id


class AgentBase:
    """
    Agent connection base class
    """

    def __init__(self, ip, port, core, proto=None, agent_type=None, agent_id=None):
        self.ip = ip
        self.port = port
        self.proto = proto
        self.agent_type = agent_type
        self.id = AgentId().get_next() if not agent_id else agent_id
        self.core = core
        self.status = AgentStatus.OFFLINE
        self.lastinfo = ''
        self.lastinfotime = 0
        self.watcher = None

    def make_http_request(self, command, **params):
        """ prepare http request """
        query_dict = {"msg": {"command": command, "params": params}}
        if len(params) == 0:
            del query_dict["msg"]["params"]
        query_string = json.dumps(query_dict)
        headers = ["POST / HTTP/1.1",
                   "Content-Type: application/json",
                   "Content-Length: " + str(len(query_string)),
                   "", query_string]
        return "\r\n".join(headers)

    def updatestatus(self, status, info=''):
        self.status = status
        self.lastinfo = info
        self.lastinfotime = int(time.time())

    def send(self, data):
        pass

    def sendall(self, data):
        pass

    def recv(self):
        return ''

    def connect(self):
        pass

    def disconnect(self):
        pass

    def handle_accept(self, sock):
        pass

    def accept(self, sock):
        self.handle_accept(sock)

    def request(self, command, **params):
        """
        make request to remote agent and wait response
        """
        try:
            self.connect()
        except socket.error as err:
            self.updatestatus(AgentStatus.OFFLINE)
            raise api.nxerrors.ConnectionError(self.ip, str(err))

        http_request = self.make_http_request(command, **params)
        logging.debug('sending %d bytes', len(http_request))
        self.sendall(http_request)
        reply = AgentReply()
        all_received = False
        try:
            while True:
                data = self.recv()
                if data:
                    all_received = reply.add_chars(data)
                    if all_received:
                        break
                else:
                    # no more data
                    break
            if not all_received:
                raise api.nxerrors.ConnectionError(self.ip, 'Lost data')

        except socket.error as err:
            raise api.nxerrors.ConnectionError(self.ip, str(err))

        finally:
            self.disconnect()
        if reply.code != '200':
            raise api.nxerrors.RequestError(reply.code, reply.error)
        json_data = json.loads(reply.content)
        if json_data['status'] == "fail":
            error = json_data['error']
            reason = error["reason"]
            side = error["code_str"]
            raise api.nxerrors.ReplyError(side, reason)
        return json_data


class AgentTcp(AgentBase):
    """
    Tcp implementation
    """
    sock = None
    accepted = False

    def __init__(self, ip, port, core, atype=None, agent_id=None):
        AgentBase.__init__(self, ip, port, core,
                           proto='tcp', agent_type=atype, agent_id=agent_id)

    def connect(self):
        if not self.accepted:
            self.sock = socket.socket()
            self.sock.settimeout(7)
            self.sock.connect((self.ip, self.port))

    def disconnect(self):
        if not self.accepted:
            logging.info("Tcp disconnected %s", self.ip)
            self.sock.close()
            self.sock = None

    def close(self):
        if not self.sock is None:
            logging.info("Tcp closed %s", self.ip)
            self.sock.close()

    def handle_accept(self, sock):
        self.sock = sock
        self.accepted = True

    def send(self, data):
        return self.sock.send(data)

    def sendall(self, data):
        return self.sock.sendall(data)

    def recv(self):
        return self.sock.recv(2048)


class AgentSsl(AgentBase):
    """ SSL implementation """

    def __init__(self, ip, port, core, ssl_cert, ssl_key, ssl_ca,
                 atype, agent_id=None):
        AgentBase.__init__(self, ip, port, core, agent_type=atype, proto='ssl',
                           agent_id=agent_id)
        self.ssl_cert = ssl_cert
        self.ssl_key = ssl_key
        self.ssl_ca = ssl_ca
        self.sock = None
        self.wrapped = None
        self.accepted = False
        self.check_files()

    def check_file(self, name):
        if not os.path.exists(name):
            raise IOError(ENOENT, 'File not found', name)
        if not os.path.isfile(name):
            raise IOError(ENOENT, 'Not a file', name)

    def check_files(self):
        self.check_file(self.ssl_ca)
        self.check_file(self.ssl_cert)
        self.check_file(self.ssl_key)

    def send(self, data):
        return self.wrapped.send(data)

    def sendall(self, data):
        return self.wrapped.sendall(data)

    def recv(self):
        return self.wrapped.recv()

    def connect(self):
        if not self.accepted:
            self.sock = socket.socket()
            self.wrapped = ssl.wrap_socket(self.sock, self.ssl_key, self.ssl_cert,
                                           ca_certs=self.ssl_ca)
            self.wrapped.settimeout(7)
            self.wrapped.connect((self.ip, self.port))

    def disconnect(self):
        if not self.accepted:
            logging.info("SSL disconnected %s", self.ip)
            self.wrapped.close()

    def handle_accept(self, sock):
        self.sock = sock
        self.wrapped = ssl.wrap_socket(self.sock, self.ssl_key, self.ssl_cert,
                                       ca_certs=self.ssl_ca, server_side=True)
        self.accepted = True


class AgentCollection:

    def __init__(self):
        self.agents = []

    def __len__(self):
        return len(self.agents)

    def add_agent(self, agent):
        self.agents.append(agent)

    def get_agent(self, ip):
        for agent in self.agents:
            if agent.ip == ip:
                return agent
        return None

    def remove_agent(self, agent_id):
        self.agents = filter(lambda x: x.id != agent_id, self.agents)

    def get(self, agent_id):
        for agent in self.agents:
            if agent.id == int(agent_id):
                return agent
        raise api.nxerrors.Missed('agent', agent_id)


def make_agent(addr, port, core, proto='tcp', ssl_cert=None,
               ssl_key=None, ssl_ca=None, atype='active', agent_id=None):
    if proto == 'ssl':
        return AgentSsl(addr, port, core, ssl_cert, ssl_key, ssl_ca,
                        atype=atype, agent_id=agent_id)
    else:
        return AgentTcp(addr, port, core, atype=atype, agent_id=agent_id)


class AgentListener(asyncore.dispatcher):
    """
    Class to listen socket and wait incoming agent connections
    """

    def __init__(self, lconf, core):
        asyncore.dispatcher.__init__(self)
        self.core = core
        self.ip = lconf.ip
        self.port = lconf.port
        self.proto = lconf.proto
        self.ssl_cert = lconf.ssl_cert if self.proto == 'ssl' else None
        self.ssl_key = lconf.ssl_key if self.proto == 'ssl' else None
        self.ssl_ca = lconf.ssl_ca if self.proto == 'ssl' else None

        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((self.ip, self.port))
        self.listen(10)

    def make_agent(self, addr, port, core, agent_id=None):
        return make_agent(addr, port, core,
                          proto=self.proto,
                          ssl_cert=self.ssl_cert,
                          ssl_key=self.ssl_key,
                          ssl_ca=self.ssl_ca,
                          atype='active',
                          agent_id=agent_id)

    def handle_accept(self):
        sock, (addr, port) = self.accept()
        logging.info('accept connection for for %s://%s:%d',
                     self.proto, addr, port)

        old_agent = self.core.agents.get_agent(addr)
        old_agent_id = None
        if old_agent:
            old_agent_id = old_agent.id
            logging.debug("agent allready exists, %d", old_agent_id)
            self.core.agents.remove_agent(old_agent_id)

        agent = self.make_agent(addr, port, self.core, agent_id=old_agent_id)
        agent.accept(sock)
        self.core.agents.add_agent(agent)
        result = agent.request('serverInfo')
        info = result['data']['server-info']
        agent.updatestatus(api.remote.AgentStatus.ONLINE, json.dumps(info))
