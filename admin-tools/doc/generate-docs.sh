#!/bin/sh -e

# Generate revdate
MONTH="--attribute month=$(date -u +%B)"
YEAR="--attribute year=$(date -u +%Y)"

my_output_dir="${MY_SCRIPT_PATH}/../doc/output"
if [ ! -d ${my_output_dir} ]; then
  mkdir -p ${my_output_dir}
else
  rm -rf ${my_output_dir}/*
fi

# Generate HTML format
asciidoctor $MONTH $YEAR -D ${my_output_dir} index.adoc

# Generate PDF format
asciidoctor-pdf $MONTH $YEAR -D ${my_output_dir} index.adoc
# Wait for generation to finish
wait
