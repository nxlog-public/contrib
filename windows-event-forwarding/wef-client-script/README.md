This script configures a Windows client to forward events to a Windows Event Collector and requires:

* A PFX certificate file containing the CA certificate and the client certificate with private key.
* The Group Policy Object Utility executable file.
* A registry policy file containing the Event Forwarding Subscription Manager settings.

Generate the PFX certificate file according to the requirements specified in the documentation for [Configuring WEF to use HTTPS](https://nxlog.co/documentation/nxlog-user-guide/im_wseventing.html#im_wseventing-https-setup).

Steps to prepare the local policy files:

1. Download the [Local Group Policy Object Utility](https://techcommunity.microsoft.com/t5/microsoft-security-baselines/lgpo-exe-local-group-policy-object-utility-v1-0/ba-p/701045).
2. Open `wef-policy.txt` with a text editor and update the settings of the Subscription Manager policy.
3. Use `LGPO.exe` to create a registry policy file:

   ```
   > LGPO.exe /r wef-policy.txt /w wef.pol
   ```

Execute the script with the following parameters:

```
> ./configure-client-wef.ps1 -CertPfxPath <path_to_pfx_file> -CertPassword <password> -LGPOTool <path_to_lgpo_exe> -PolicyFile <path_to_reg_policy_file>
```

Example to configure certificate-based authentication only:

```
> ./configure-client-wef.ps1 -CertPfxPath C:\Wef-Script\client.pfx -CertPassword secret
```

Example to configure the local policy only:

```
> ./configure-client-wef.ps1 -LGPOTool C:\Wef-Script\LGPO.exe -PolicyFile C:\Wef-Script\wef.pol
```

This script is provided "AS IS" without warranty of any kind, either expressed or implied. Use at your own risk.
