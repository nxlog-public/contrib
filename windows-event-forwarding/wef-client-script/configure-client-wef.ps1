<#

configure-client-wef.ps1

Description:

    This script configures a Windows client to forward events to a
    Windows Event Collector.
    
    For certificate-based authentication:

    1. Sets the Windows Remote Management service to automatic startup type.
    2. Starts the Windows Remote Management service if not running.
    3. Enables certificate authentication for Windows Remote Management
    4. Imports the certificates to the certificate store of the Local Computer.
    5. Adds the Network Service account to the Event Log Readers group.

    For configuring event forwarding in the Local Computer policy:

    1. Configures the policy using the Local Group Policy Object Utility tool.
    2. Applies the policy changes.

Parameters:
    -CertPfxPath           Path to the .pfx file containing the CA and 
                           client certificates.
    -CertPassword          Password for the .pfx file.
    -LGPOTool              Path to the Local Group Policy Object Utility tool.
    -PolicyFile            Path to the .pol file containing Event Forwarding
                           Subscription Manager settings.

See the README.md for setup instructions.

#>

param (
    [string]$CertPfxPath,
    [string]$CertPassword,
    [string]$LGPOTool = ".\LGPO.exe",
    [string]$PolicyFile
)

if ([IntPtr]::Size -eq 4) {
    $regPath = 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*'
}
else {
    $regPath = @(
        'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*'
        'HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
    )
}

Start-Transcript -Path configure-client-wef.log

if ($CertPfxPath)
{
    <#
      Configure Windows Remote Management service.
    #>

    Write-Host("Setting the Windows Remote Management service to automatic startup") 
    Set-Service WInRM -StartupType Automatic

    if ((Get-Service "WinRM").Status -ne "Running") 
    { 
        Write-Host("Starting the Windows Remote Management service")
        Start-Service "WinRM"
        (Get-Service "WinRM").WaitForStatus("Running", '00:00:10')

        if ((Get-Service "WinRM").Status -ne "Running") 
        {
            Write-Host("Failed to start service")
        }
        else
        {
            Write-Host("Service started successfully")
        }
    }
    else
    {
         Write-Host("Windows Remote Management service already started")
    }

    Write-Host("Enabling certificate authentication for Windows Remote Management")
    winrm set winrm/config/client/auth '@{Certificate="true"}'

    <#
      Import certificates to the LocalMachine certificate store.
    #>

    function Add-Certificate([String]$CertStore, [System.Security.Cryptography.X509Certificates.X509Certificate2]$Cert)
    {
        try {
            $store = [System.Security.Cryptography.X509Certificates.X509Store]::new($certStore, $certRootStore);
            $store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite);

            if ($cert.Thumbprint -in @($store.Certificates | % { $_.Thumbprint } )) {
                Write-Warning "Certificate is already in the store, removing..." 
                $store.Remove($cert)
            }
	
            $store.Add($cert);
        } finally {
            if($store) {
                $store.Dispose()
            }
        }
    }

    $certRootStore = "LocalMachine"
    $certFlags = [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::MachineKeySet `
            -bor [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::PersistKeySet

    $collection = [System.Security.Cryptography.X509Certificates.X509Certificate2Collection]::new();
    $collection.Import($CertPfxPath, $CertPassword, $certFlags);

    foreach ($cert in $collection) {
        Write-Host ("Importing certificate subject: '{0}'" -f  $cert.Subject  )

        if ($cert.HasPrivateKey)
        {
            # Import client certificate to the Personal store
            Add-Certificate -CertStore "My" -Cert $cert

            # Grant the Network Service account permissions to read the private key
            $rsaCert = [System.Security.Cryptography.X509Certificates.RSACertificateExtensions]::GetRSAPrivateKey($cert)
            $fileName = $rsaCert.key.UniqueName
            $path = "$env:ALLUSERSPROFILE\Microsoft\Crypto\RSA\MachineKeys\$fileName"
            $permissions = Get-Acl -Path $path

            $access_rule = New-Object System.Security.AccessControl.FileSystemAccessRule("NT AUTHORITY\NETWORK SERVICE", 'Read', 'None', 'None', 'Allow')
            $permissions.AddAccessRule($access_rule)
            Set-Acl -Path $path -AclObject $permissions
        }
        else
        {
            # Import CA certificate to the Trusted Root CA store
            Add-Certificate -CertStore "Root" -Cert $cert
        }
    }

    <#
      Add Network Service user to Event Log Readers group.
    #>

    $group = "Event Log Readers"
    $user = "NT AUTHORITY\NETWORK SERVICE"

    if ((Get-LocalGroupMember $group).Name -contains $user)
    {
        Write-Host("Network Service user is already a member of the Event Log Readers Group")
    }
    else
    {
        Write-Host("Adding Network Service user to the Event Log Readers group")
        Add-LocalGroupMember -Group $group -Member $user
    }
}

if ($PolicyFile)
{
    <#
      Configure event forwarding in the Local Computer Policy.
    #>

    Write-Host("Configuring Local Computer Policy")
    $lgpoToolArgs = "/m " + $PolicyFile
    $cmd = "& '$LGPOTool' $lgpoToolArgs"
    Invoke-Expression $cmd

    gpupdate /force
}

Stop-Transcript