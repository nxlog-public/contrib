The XML Schema Definition (XSD) for the pattern database. See more details in the official documentation: https://nxlog.co/documentation/nxlog-user-guide/xm_pattern.html
