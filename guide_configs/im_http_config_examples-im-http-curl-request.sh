URL=https://<hostname>
PORT=<port>
USESSL="--cert keys/client-cert.pem --key keys/client-key.pem --cacert keys/ca.pem"

BYTELEN=`printf "%s" "$1" | wc -c`

curl -v $USESSL -H "Content-Type:plain/text" -H "Content-Length:${BYTELEN}" -d "$1" $URL:$PORT