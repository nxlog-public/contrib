#!/bin/bash
CRS=/opt/nxlog/var/spool/nxlog/systemd-tail.pos
SAMPLE_TIME=1

while sleep $SAMPLE_TIME; do
  journalctl -o json --cursor-file=${CRS} --no-pager
done