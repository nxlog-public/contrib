import binascii, collections, datetime, nxlog, pyodbc
from azure.storage.blob import PageBlobService

################################################################################

# Update these parameters.

# MSSQL details
DRIVER = "{ODBC Driver 13 for SQL Server}"
SERVER = 'tcp:XXXXXXXX.database.windows.net'
DATABASE = 'XXXXXXXX'
USERNAME = 'XXXXXXXX@XXXXXXXX'
PASSWORD = 'XXXXXXXX'

# Azure Storage details
STORAGE_ACCOUNT = 'XXXXXXXX'
STORAGE_KEY = 'XXXXXXXX=='
CONTAINER_NAME = 'sqldbauditlogs'

# Log pull time interval in seconds
TIME_INTERVAL = 300

# The timestamp is saved to this file for resuming.
CACHE_FILE = '/opt/nxlog/var/spool/nxlog/nxlog_mssql_auditlog_position.txt'

################################################################################

# Save position to the cache file
def save_position(timestamp):
    # Truncate temp file and open for writing
    with open(CACHE_FILE, 'w+') as f:
        f.write(str(timestamp))

# Read the last time of log query, return None if cache file is empty
def read_position():
    # Open or create a new empty temp file
    with open(CACHE_FILE, 'a+') as f:
        line = f.readline()
        if line != "":
            position = datetime.datetime.strptime(line, "%Y-%m-%d %H:%M:%S.%f")
        else:
            position = None
    return position

# Get name of a Blob with the latest audit file from Azure Storage
def get_blob():
    # Connect to Blob Service
    page_blob_service = PageBlobService(account_name=STORAGE_ACCOUNT,
                                            account_key=STORAGE_KEY)
    # List all blobs in the container
    generator = page_blob_service.list_blobs(CONTAINER_NAME)
    # Get the latest blob
    last_blob = list(page_blob_service.list_blobs(CONTAINER_NAME))[-1]
    latest_audit_log = last_blob.name
    # Construct blob URL
    blob = "https://{}.blob.core.windows.net/{}/{}".format(STORAGE_ACCOUNT,
                                                              CONTAINER_NAME,
                                                              latest_audit_log)
    return blob


# Build an ordered dictionary from a database row
def build_event(row):
    event = [
        ('EventTime', row[0]),
        ('SequenceNumber', row[1]),
        ('ActionId', row[2]),
        ('Succeeded', row[3]),
        ('PermissionBitmask', binascii.hexlify(row[4])),
        ('IsColumnPermission', row[5]),
        ('SessionId', row[6]),
        ('ServerPrincipalId', row[7]),
        ('DatabasePrincipalId', row[8]),
        ('TargetServerPrincipalId', row[9]),
        ('TargetDatabasePrincipalId', row[10]),
        ('ObjectId', row[11]),
        ('ClassType', row[12]),
        ('SessionServerPrincipalName', row[13]),
        ('ServerPrincipalName', row[14]),
        ('ServerPrincipalSid', binascii.hexlify(row[15])),
        ('DatabasePrincipalName', row[16]),
        ('TargetServerPrincipalName', row[17]),
        ('TargetServerPrincipalSid', row[18]),
        ('TargetDatabasePrincipalName', row[19]),
        ('ServerInstanceName', row[20]),
        ('DatabaseName', row[21]),
        ('SchemaName', row[22]),
        ('ObjectName', row[23]),
        ('Statement', row[24]),
        ('AdditionalInformation', row[25]),
        ('FileName', row[26]),
        ('AuditFileOffset', row[27]),
        ('UserDefinedEventID', row[28]),
        ('UserDefinedInformation', row[29]),
        ('AuditSchemaVersion', row[30]),
        ('SequenceGroupId', binascii.hexlify(row[31])),
        ('TransactionId', row[32]),
        ('ClientIp', row[33]),
        ('ApplicationName', row[34]),
        ('DurationMilliseconds', row[35]),
        ('ResponseRows', row[36]),
        ('AffectedRows', row[37])
    ]
    return collections.OrderedDict(event)

# Collect log data
def read_logs(query_start, query_end, module):

    # Get Blob name from Azure Cloud
    blob = get_blob()

    # Connect to MSSQL database
    cnxn = pyodbc.connect(
        "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format(DRIVER,
                                                               SERVER,
                                                               DATABASE,
                                                               USERNAME,
                                                               PASSWORD))
    cursor = cnxn.cursor()

    # Query Blob file
    cursor.execute(
        """SELECT * FROM sys.fn_get_audit_file(?, default,default)
        WHERE event_time > ? AND event_time <= ?""",
        blob, str(query_start),str(query_end))
    row = cursor.fetchone()

    while row:
        event = build_event(row)
        kv_pairs = ', '.join(['{}={}'.format(k, event[k]) for k in event])
        logdata = module.logdata_new()
        logdata.set_field('raw_event', kv_pairs)
        for k in event:
            logdata.set_field(k, str(event[k]))
        logdata.post()
        row = cursor.fetchone()
    cursor.close()

    # Save end time
    module['end'] = query_end
    save_position(query_end)

def read_data(module):
    # Get last run time
    if 'end' in module:
        nxlog.log_info("Last run finished at "+ str(module['end']))
    else:
        module['end'] = read_position()

    # Get start time for new query
    if module['end'] is None:
        query_start =  datetime.datetime.now() - datetime.timedelta(seconds=TIME_INTERVAL)
    else:
        query_start = module['end']

    # Get end time for new query; use a delay in case of time differences
    query_end = datetime.datetime.now() - datetime.timedelta(seconds=5)

    # Run the query for logs
    read_logs(query_start, query_end, module)
    # Set next run of the module
    module.set_read_timer(TIME_INTERVAL)

nxlog.log_info("INIT SCRIPT")