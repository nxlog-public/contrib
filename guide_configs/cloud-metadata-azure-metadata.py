import json, nxlog, requests

def request_metadata():
    """Gets all metadata values for compute instance, returns dict"""
    # Set metadata URL
    metaurl = 'http://169.254.169.254/metadata/instance/compute?api-version=2017-08-01'
    # Set header required to retrieve metadata
    metaheader = {'Metadata':'true'}

    # Send HTTP GET request
    r = requests.get(metaurl, headers=metaheader)

    # If present, get text payload from the response
    if r.status_code != 404:
        value = r.text
    else:
        value = None

    # Load JSON data into Python dictionary and return
    return json.loads(value)

def get_attribute(event):
    """Reads metadata and stores as event fields"""
    # Get nxlog module object
    module = event.module

    # Request for metadata only if not already present in the module
    if 'metadata' not in module:
        module['metadata'] = request_metadata()

    # Get metadata stored in module object
    metadata = module['metadata']

    # Save attributes and their values as event fields
    for attribute in metadata:
        event.set_field(attribute, metadata[attribute])