
# ---------------- REGULAR EXPRESSIONS FOR PARSING DATA ------------------------

define DATA_MANAGER_REGEX /(?x)^(\d+.\d+.\d+)\s+(\d+:\d+:\d+).\d+\
                          \s+PID=(?<PID>\d+)\s+TID=(?<TID>\d+)\s+\
                          (?<ParamFive>\w+)\s+\S(?<Loader>\w+):\s+\
                          (?<Message>.*)/

define CHANNEL_REGEX      /(?x)^(\d+-\d+-\d+)\s(\d+:\d+:\d+),\
                          \d+\s+(?<Type>\w+)\s+(?<Message>.*)/

define DELTA_REGEX        /(?x)^(?<Loader>[a-zA-Z.]*)\s+:\s+(\d+.\d+.\d+)\
                          \s(\d+:\d+:\d+)\s:\s(?<Message>[\w():.\\\s]*)/

define OPC_UA_LOG_HEADER  /(?x)^\d+\/\d+\/\d+\s+\d+:\d+:\d+\s+[\d\w\s:]*\d+/

define PM_REGEX           /(?x)^(\d+-\d+-\d+)\s+(\d+:\d+:\d+).\d+\s+\
                          (?<Type>\w+)\(\s*(?<PID>\d*)\-(?<TID>\d*)\)\
                          \s*(?<Message>.*)/

define TRACE_REGEX        /(?x)^[^\w\d]*(\d*);\s*(\d*-\d*\
                          -\d*\w\d*:\d*:\d*.\d*\w);\s*(?<PID>\d*);\
                          \s*(?<TID>\d*);\s*\[(?<Address>\w*\d*)\];\
                          \s(?<Process>[\w\\]*);\s*(?<ParamSeven>\w*)\
                          ;(?<MessageOne>.*);\s*\"(?<MessageTwo>.*)\"/

define EVENT_REGEX        /(?x)^(\d+.\d+.\d+)\s+(\d+:\d+:\d+)\s+(?<Message>.*)/

define STARTUP_REGEX      /(?x)^(\d+),(\d+.\d+.\d+),(\d+:\d+:\d+):\
                          \d+,(\d+),(\d+),,(?<Station>.*),\
                          (?<Runtime>.*),(?<Message>.*),(?<State>.*)/

define EVENT_SQLAGENT     /(?x)^(\d+-\d+-\d+)\s+(\d+:\d+:\d+)\
                          \s+-\s+\W\s+(?<Message>.*)/

define ARCHIVE_REGEX      /(?x)^(\d+-\d+-\d+)\s+(\d+:\d+:\d+).\
                          \d+\s+\[p=(?<PID>\d+),\s+t=(?<TID>.*)\
                          \]\s+(?<Message>.*)/

define REGEX_LP_HEADER    /(?x)^\s?\**(?<Type>[\w ]*)\<(?<Path>\
                          [-_ \\\/\(\)\d*\w*]*)\>\s*on\s*(?<Date>\
                          \d+\/\d+\/\d+)\s*(?<Time>\d+:\d+:\d+)/

define REGEX_LP_ONE       /(?x)^\[?(?<Time>\d+:\d+:\d+.\d+)\s*\
                          (?<Daypart>AM|PM)\]?\s*(?<Process>\
                          [._ \(\)~@\-\w]*)\[(?<ParamFour>[\w\d]*)\]\
                          \s*\((?<Status>[\w]*)\)/

define REGEX_LP_TWO       /(?x)^\[?(?<Time>\d+:\d+:\d+.\d+)\s*\
                          (?<Daypart>AM|PM)\]?\s*(?<Process>.*)\
                          \s*\((?<Status>.*)\)/

define REGEX_LR_HEADER    /(?x)^\s?\**(?<Type>[\w ]*)\<(?<Path>\
                          [-_ \\\/\(\)\d*\w*]*)\>\s*on\s*(?<Date>\
                          \d+\/\d+\/\d+)\s*(?<Time>\d+:\d+:\d+)/

define REGEX_LR_ONE       /(?x)^(?<ParamOne>[\(\)@\d\w_]*)\s*\
                          \[(?<ParamTwo>[\w\d]*)\]\s*\((?<ParamThree>[\w]\
                          *\s*)\s*?\)\s*->\s*\((?<ParamFour>[\w]*\s*)\)\s*\
                          ->\s(?<ParamFive>[\(\)@\w_.-]*)\s*(?:\[(?<ParamSix>\
                          [\w\d]*)\])?\s*\((?<ParamSeven>[\w]*)\s*\)/

define ASSIST_REGEX_HEAD  /(?x)^(?<Message>.*)from:\s*(?<DayOfWeek>\w*),\
                          \s*(?<Month>\w*)\s*(\d*),\s*(\d*)\
                          \s*(?<Time>\d*:\d*:\d*)/

define ASSIST_REGEX_LOG   /(?x)^(?<Path>.*)\s+(?<Status>OK|ERROR|Error)\
                          \s+(?<Message>.*)/

# ---------------- PATHS TO LOG FILES ------------------------------------------

define DIAGNOSE_PATH      C:\Program Files (x86)\SIEMENS\WinCC\diagnose

define OPC_UA_PATH        C:\ProgramData\Siemens\Automation\Logfiles

define TRACE_PATH         C:\ProgramData\Siemens\Logs\REGSVR32\trace

define SQL_SERVER_PATH    C:\Program Files (x86)\Microsoft SQL Server

define PROJECT_PATH       C:\Users\Engineer\Documents\Project\BEV_MP

# ---------------- EXTENSION MODULES -------------------------------------------

<Extension json>
    Module        xm_json
</Extension>

<Extension _charconv>
    Module        xm_charconv
    LineReader    UCS-2LE
</Extension>

<Extension _multi_opc_ua>
    Module        xm_multiline
    HeaderLine    %OPC_UA_LOG_HEADER%
</Extension>

<Extension _multi_shared>
    Module        xm_multiline
    HeaderLine    /^Synchronization.*on\s+\d+.\d+.\d+\s+\d+:\d+:\d+$/
</Extension>

<Extension _multi_update_block>
    Module        xm_multiline
    HeaderLine    /^.*:$/
</Extension>

# ---------------- INPUT MODULES -----------------------------------------------

<Input data_manager>
    Module        im_file
    File          '%DIAGNOSE_PATH%\CCDmRtServer.log'
    InputType     _charconv
    <Exec>
        if $raw_event =~ %DATA_MANAGER_REGEX%
        {
            $EventTime = strptime($1 + $2,"%d.%m.%Y %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input channel>
    Module        im_file
    File          '%DIAGNOSE_PATH%\SIMATIC_S7_Protocol_Suite_*.log'
    <Exec>
        if $raw_event =~ %CHANNEL_REGEX%
        {
            $EventTime = strptime($1 + $2,"%Y-%m-%d %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input delta>
    Module        im_file
    File          '%DIAGNOSE_PATH%\CCDeltaLoader.Log'
    InputType     _charconv
    <Exec>
        if $raw_event =~ %DELTA_REGEX%
        {
            $EventTime = strptime($2 + $3,"%d.%m.%Y %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input opc_ua>
    Module        im_file
    File          '%OPC_UA_PATH%\sinec2.log'
    InputType     _multi_opc_ua
    <Exec>
        $Message = $raw_event;
        $Message = replace($Message, "\r\n" , " ");
        $Message = replace($Message, "\t" , " ");
        to_json();
    </Exec>
</Input>

<Input pm>
    Module        im_file
    File          '%DIAGNOSE_PATH%\PM*.log'
    <Exec>
        if $raw_event =~ %PM_REGEX%
        {
            $EventTime = strptime($1 + $2,"%Y-%m-%d %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input trace>
    Module        im_file
    File          '%TRACE_PATH%\*'
    <Exec>
        if $raw_event =~ %TRACE_REGEX%
        {
            $EntryNumber = integer($1);
            $Timestamp = parsedate($2);
            to_json();
        }
		else drop();
    </Exec>
</Input>

<Input surrogate>
    Module        im_file
    File          '%DIAGNOSE_PATH%\CUCSurrogate.log'
    <Exec>
        if $raw_event =~ %EVENT_REGEX%
        {
            $EventTime = strptime($1 + $2,"%d.%m.%Y %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input startup>
    Module        im_file
    File          '%DIAGNOSE_PATH%\WinCC_SStart_*.log'
    InputType     _charconv
    <Exec>
        if $raw_event =~ %STARTUP_REGEX%
        {
            $ID = integer($1);
            $EventTime = strptime($2 + $3,"%d.%m.%Y%T");
            $MessageNumber = integer($4);
            $MessageClass = integer($5);
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input sqlagent>
    Module        im_file
    File          '%SQL_SERVER_PATH%\MSSQL12.WINCC\MSSQL\Log\SQLAGENT.*'
    InputType     _charconv
    <Exec>
        if $raw_event =~ %EVENT_SQLAGENT%
        {
            $EventTime = strptime($1 + $2, "%Y-%m-%d %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input archive>
    Module        im_file
    File          '%PROJECT_PATH%\Hmi\wincproj\HMI\UA\UALogFile.txt'
    <Exec>
        if $raw_event =~ %ARCHIVE_REGEX%
        {
            $EventTime = strptime($1 + $2,"%Y-%m-%d %T");
            to_json();
        }
        else drop();
    </Exec>
</Input>

<Input loadprot>
    Module        im_file
    File          '%PROJECT_PATH%\BEV_Prj\ES_LOC\1\GEN\loadprot.log'
    <Exec>
        if $raw_event =~ %REGEX_LP_HEADER% to_json();
        else if $raw_event =~ %REGEX_LP_ONE% to_json();
        else if $raw_event =~ %REGEX_LP_TWO% to_json();
        else drop();
    </Exec>
</Input>

<Input loadref>
    Module        im_file
    File          '%PROJECT_PATH%\VR_Lib\ES_LOC\1\GEN\loadrefs.log'
    <Exec>
        if $raw_event =~ %REGEX_LR_HEADER% to_json();
        else if $raw_event =~ %REGEX_LR_ONE% to_json();
        else drop();
    </Exec>
</Input>

<Input shared_declarations>
    Module        im_file
    File          '%PROJECT_PATH%\BEV_MP\Global\GDAlignError.log'
    InputType     _multi_shared
    <Exec>
        $Message = $raw_event;
        $Message = replace($Message, "\r\n" , " ");
        to_json();
    </Exec>
</Input>

<Input import_export>
    Module        im_file
    File          '%PROJECT_PATH%\BEV_Lib\Global\modify.log'
    <Exec>
        if $raw_event =~ %ASSIST_REGEX_HEAD%
        {
            $Date = integer($4);
            $Year = integer($5);
            to_json();
        }
        else if $raw_event =~ %ASSIST_REGEX_LOG% to_json();
        else drop();
    </Exec>
</Input>

<Input update_block>
    Module        im_file
    File          '%PROJECT_PATH%\BEV_Lib\@CentralBstActualize*.TXT'
    InputType     _multi_update_block
    <Exec>
        $Message = $raw_event;
        $Message = replace($Message, "-> ", " ");
        $Message = replace($Message, "\r\n", " ");
        to_json();
    </Exec>
</Input>

<Output to_file>
    Module        om_file
    File          'C:\output.txt'
</Output>

# ---------------- ROUTES ------------------------------------------------------

<Route r1>
    Path data_manager, channel, delta, opc_ua, pm, trace => to_file
</Route>

<Route r2>
    Path surrogate, startup, sqlagent, archive, loadprot => to_file
</Route>

<Route r3>
    Path loadref, shared_declarations, import_export => to_file
</Route>

<Route r4>
    Path update_block => to_file
</Route>