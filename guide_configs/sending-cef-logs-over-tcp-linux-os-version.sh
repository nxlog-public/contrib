#!/bin/bash
OSHOST=$(cat /etc/os-release | grep -oP '^NAME=.*' | grep -oP '"(.*)"' | sed s/\"//g)
OSVERSION=$(cat /etc/os-release | grep VERSION= | grep -oP '"(.*)"' | sed s/\"//g)
OSNAME="$OSHOST $OSVERSION"
echo "define OSNAME $OSNAME"