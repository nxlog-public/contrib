# If running 32-bit on a 64-bit system, run 64-bit PowerShell instead.
if ( $env:PROCESSOR_ARCHITEW6432 -eq "AMD64" ) {
  Write-Output "Running 64-bit PowerShell."
  &"$env:SYSTEMROOT\SysNative\WindowsPowerShell\v1.0\powershell.exe" `
  -NonInteractive -NoProfile -ExecutionPolicy Bypass `
  -File "$($myInvocation.InvocationName)" $args
  exit $LASTEXITCODE
}
################################################################################

# Update these parameters.

# The path to MSSQL Server DLLs
$SharedPath = "C:\Program Files\Microsoft SQL Server\140\Shared";

# The path to local working directory
$localTargetDirectory = "C:\temp\"

# Azure details: credentials, resource group, storage account, container
$accountName ="<YOUR ACCOUNT NAME>"
$password = ConvertTo-SecureString "<YOUR PASSWORD>" -AsPlainText -Force
$resourceGroup = "<YOUR SECOURCE GROUP>"
$storageAccountName = "<YOUR STORAGE ACCOUNT NAME>"
$containerName = "sqldbauditlogs"

# The timestamp is saved to this file for resuming.
$CacheFile = 'C:\nxlog_mssql_auditlog_position.txt'

# The audit file is downloaded and read at this interval in seconds.
$PollInterval = 3600

# Allow this many seconds for new logs to be written to the audit file.
$ReadDelay = 30

################################################################################
# Get audit data from $containerName container in $storageAccountName Azure storage
# in range $start to $end.
function Get-Audit-Data {
   param( $accountName, $password, $resourceGroup, $storageAccountName, $containerName, $start, $end )

   $xeCore = [System.IO.Path]::Combine($SharedPath,
       "Microsoft.SqlServer.XE.Core.dll");
   $xeLinq = [System.IO.Path]::Combine($SharedPath,
       "Microsoft.SqlServer.XEvent.Linq.dll");
   Add-Type -Path $xeLinq;

   # Notes on "Microsoft.SqlServer.XE.Core.dll":
   #  • For SQL 2014, it is a dependency of "Microsoft.SqlServer.XEvent.Linq.dll".
   #  • For SQL 2012, the file does not exist.
   if( [System.IO.File]::Exists($xeCore) ) { Add-Type -Path $xeCore; }

   # Log in to Azure account
   $credential = New-Object System.Management.Automation.PSCredential($accountName, $password)
   Login-AzureRMAccount -Credential $credential

   # Get Azure storage account
   $storageAccount = Get-AzureRmStorageAccount -ResourceGroupName $resourceGroup -AccountName $storageAccountName
   $ctx = $storageAccount.Context

   # Download latest audit file
  $lastAuditFile = Get-AzureStorageBlob -Container $containerName -Context $ctx | select Name | select -Last 1
  $blobName = $lastAuditFile.Name
  Get-AzureStorageBlobContent -Blob $blobName `
    -Container $containerName `
    -Destination $localTargetDirectory `
    -Context $ctx

  # Get path to the local file
  $blobName = $localTargetDirectory+$blobName | % { [System.Io.Path]::GetFullPath($_) }

  [Microsoft.SqlServer.XEvent.Linq.QueryableXEventData] $xEvents = $null;

  Try {
       # Get events from audit file
       $xEvents =
         New-Object -TypeName Microsoft.SqlServer.XEvent.Linq.QueryableXEventData(
           $blobName
         )

        # These fields contain binary arrays, so they need special handling
        $binaryValues = @("permission_bitmask","server_principal_sid","target_server_principal_sid")

       ForEach($publishedEvent in $xEvents) {
         # Read only events from latest time period
         if(($publishedEvent.Timestamp -ge $start) -and ($publishedEvent.Timestamp -le $end)) {

           # Create hash table
           $record = @{}

           ForEach ($fld in $publishedEvent.Fields) {

                # Check for binary arrays
                if($binaryValues.Contains($fld.Name)){
                  # Convert binary arrays to hex
                  $value = ($fld.Value |ForEach-Object ToString X2) -join ''
                }
                else {
                  # Convert to string
                  $value = $fld.Value.ToString();
                }
                $record += @{$fld.Name = $value }
           }
           # Return record as JSON
           $record | ConvertTo-Json -Compress | Write-Output

         }
       }
     }
     Catch {
       Write-Host "Exception Message: $($_.Exception.Message)"
     }
     Finally {
       if ($xEvents -is [IDisposable]) {
         $xEvents.Dispose();
       }
     }

     # Remove downloaded XEL file
     Remove-Item $blobName
}

# Get position timestamp from cache file. On first run, create file using
# current time.
function Get-Position {
    param( $file )
    Try {
        if (Test-Path $file) {
            $timestamp = (Get-Date (Get-Content $file -First 1))
            $timestamp = $timestamp.ToUniversalTime()
            $start = $timestamp.AddTicks(-($timestamp.Ticks % 10000000))
        }
        else {
            $timestamp = [System.DateTime]::UtcNow
            $timestamp = $timestamp.AddTicks(-($timestamp.Ticks % 10000000))
            Save-Position $file $timestamp
        }
        return $start
    }
    Catch {
        Write-Error "Failed to read timestamp from position file."
        exit 1
    }
}

# Save position timestamp to cache file.
function Save-Position {
    param( $file, $time )
    Try { Out-File -FilePath $file -InputObject $time.ToString('o') }
    Catch {
        Write-Error "Failed to write timestamp to position file."
        exit 1
    }
}

# Main
Try {
    $start = Get-Position $CacheFile
    Write-Debug "Got start time of $($start.ToString('o'))."
    $now = [System.DateTime]::UtcNow
    $now = $now.AddTicks(-($now.Ticks % 10000000))
    Write-Debug "Got current time of $($now.ToString('o'))."
    $diff = ($now - $start).TotalSeconds
    # Check whether waiting is required to comply with $ReadDelay.
    if (($diff - $PollInterval) -lt $ReadDelay) {
        $wait = $ReadDelay - $diff + $PollInterval
        Write-Debug "Waiting $wait seconds to start collecting logs."
        Start-Sleep -Seconds $wait
    }
    # Repeatedly read from the audit log
    while($true) {
        Write-Debug "Using range start time of $($start.ToString('o'))."
        $now = [System.DateTime]::UtcNow
        $now = $now.AddTicks(-($now.Ticks % 10000000))
        $end = $now.AddSeconds(-($ReadDelay))
        Write-Debug "Using range end time of $($end.ToString('o'))."
        foreach($site in $sites) { Get-Audit-Data $accountName $password $resourceGroup $storageAccountName $containerName $start $end }
        Write-Debug "Saving position timestamp to cache file."
        Save-Position $CacheFile $end
        Write-Debug "Waiting $PollInterval seconds before reading again."
        Start-Sleep -Seconds $PollInterval
        $start = $end
    }
}
Catch {
    Write-Error "An unhandled exception occurred!"
    exit 1
}