# To make NXLog return an error, write to standard error and exit 1
if ($false) {
    [Console]::Error.WriteLine("This is an error")
    exit 1
}
else {
# Anything written to standard output is used as configuration content
	$osname = (Get-WmiObject -Class Win32_OperatingSystem).caption
	Write-Output "define OSNAME $osname"
}