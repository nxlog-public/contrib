This is Signature verification script for DEB packages. 

The verification of the NXLog DEB packages requires the debsig-verify package to be installed. To install debsig-verify, run the following command:

```sh
apt install debsig-verify
```

Run the deb-verify script with the path to the NXLog deb-package as its parameter. For example, it may be the following command:

```sh
./deb-verify ../nxlog-4.8.4835_ubuntu20_amd64.deb
```

The script output should look similar to this:

```
Verified package from 'Nxlog package' (Nxlog)
```
