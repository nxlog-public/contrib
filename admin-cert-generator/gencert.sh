#!/bin/sh

SUBJ="/CN=agent-name.exmaple.com/O=nxlog.org/C=HU/ST=state/L=location"
CERTDIR=.

echo "Generating Key"
openssl req -new -newkey rsa:2048 -nodes -keyout agent-key.pem -out req.pem -batch -subj "$SUBJ" -config gencert.cnf
echo "Generating Cert"
openssl x509 -req -in req.pem -CA agent-ca.pem -CAkey agent-ca-key.pem -out agent-cert.pem -set_serial 01 -days 3650
rm -f req.pem
