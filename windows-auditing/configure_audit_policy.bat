@echo off

ver |findstr "6\.[0-3]\.* 10\.[0-1]\.*" > NUL
if not errorlevel 1 ( 
	echo Configuring Advanced Audit Policy...

	rem Account Logon
	rem Credential Validation
	auditpol.exe /set /subcategory:{0cce923f-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Kerberos Authentication Service
	auditpol.exe /set /subcategory:{0cce9242-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Kerberos Service Ticket Operations
	auditpol.exe /set /subcategory:{0cce9240-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other Account Logon Events
	auditpol.exe /set /subcategory:{0cce9241-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul

	rem Account Management
	rem Application Group Management
	auditpol.exe /set /subcategory:{0cce9239-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Computer Account Management
	auditpol.exe /set /subcategory:{0cce9236-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Distribution Group Management
	auditpol.exe /set /subcategory:{0cce9238-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other Account Management Events
	auditpol.exe /set /subcategory:{0cce923a-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Security Group Management
	auditpol.exe /set /subcategory:{0cce9237-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem User Account Management
	auditpol.exe /set /subcategory:{0cce9235-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem Detailed Tracking
	rem DPAPI Activity
	auditpol.exe /set /subcategory:{0cce922d-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem PNP Activity
	auditpol.exe /set /subcategory:{0cce9248-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Process Creation
	auditpol.exe /set /subcategory:{0cce922b-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul
	rem Process Termination
	auditpol.exe /set /subcategory:{0cce922c-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul
	rem RPC Events
	auditpol.exe /set /subcategory:{0cce922e-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Token Right Adjusted
	auditpol.exe /set /subcategory:{0cce924a-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem DS Access
	rem Detailed Directory Service Replication
	auditpol.exe /set /subcategory:{0cce923e-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Directory Service Access
	auditpol.exe /set /subcategory:{0cce923b-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Directory Service Changes
	auditpol.exe /set /subcategory:{0cce923c-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Directory Service Replication
	auditpol.exe /set /subcategory:{0cce923d-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem Logon/Logoff
	rem Account Lockout
	auditpol.exe /set /subcategory:{0cce9217-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem User / Device Claims
	auditpol.exe /set /subcategory:{0cce9247-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul
	rem Group Membership
	auditpol.exe /set /subcategory:{0cce9249-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem IPsec Extended Mode
	auditpol.exe /set /subcategory:{0cce921a-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem IPsec Main Mode
	auditpol.exe /set /subcategory:{0cce9218-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem IPsec Quick Mode
	auditpol.exe /set /subcategory:{0cce9219-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Logoff
	auditpol.exe /set /subcategory:{0cce9216-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Logon
	auditpol.exe /set /subcategory:{0cce9215-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Network Policy Server
	auditpol.exe /set /subcategory:{0cce9243-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other Logon/Logoff Events
	auditpol.exe /set /subcategory:{0cce921c-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Special Logon
	auditpol.exe /set /subcategory:{0cce921b-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem Object Access
	rem Audit Application Generated
	auditpol.exe /set /subcategory:{0cce9222-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Certification Services
	auditpol.exe /set /subcategory:{0cce9221-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Detailed File Share
	auditpol.exe /set /subcategory:{0cce9244-69ae-11d9-bed3-505054503030} /failure:enable /success:disable >nul 2>nul
	rem File Share
	auditpol /set /subcategory:{0cce9224-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem File System
	auditpol /set /subcategory:{0cce921d-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Filtering Platform Connection
	auditpol.exe /set /subcategory:{0cce9226-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul
	rem Filtering Platform Packet Drop
	auditpol.exe /set /subcategory:{0cce9225-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul
	rem Handle Manipulation
	auditpol.exe /set /subcategory:{0cce9223-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Kernel Object
	auditpol.exe /set /subcategory:{0cce921f-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other Object Access Events
	auditpol.exe /set /subcategory:{0cce9227-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul
	rem Registry
	auditpol /set /subcategory:{0cce921e-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Removable storage
	auditpol.exe /set /subcategory:{0cce9245-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem SAM
	auditpol.exe /set /subcategory:{0cce9220-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Central Access Policy Staging
	auditpol.exe /set /subcategory:{0cce9246-69ae-11d9-bed3-505054503030} /failure:disable /success:disable >nul 2>nul

	rem Policy Change
	rem Audit Policy Change
	auditpol.exe /set /subcategory:{0cce922f-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Authentication Policy Change
	auditpol.exe /set /subcategory:{0cce9230-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Authorization Policy Change
	auditpol.exe /set /subcategory:{0cce9231-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Filtering Platform Policy Change
	auditpol.exe /set /subcategory:{0cce9233-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem "MPSSVC Rule-Level Policy Change"
	auditpol.exe /set /subcategory:{0cce9232-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other Policy Change Events
	auditpol.exe /set /subcategory:{0cce9234-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem Privilege Use
	rem Non Sensitive Privilege Use
	auditpol.exe /set /subcategory:{0cce9229-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other Privilege Use Events
	auditpol.exe /set /subcategory:{0cce922a-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Sensitive Privilege Use
	auditpol.exe /set /subcategory:{0cce9228-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem System
	rem IPsec Driver
	auditpol.exe /set /subcategory:{0cce9213-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Other System Events
	auditpol.exe /set /subcategory:{0cce9214-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Security State Change
	auditpol.exe /set /subcategory:{0cce9210-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem Security System Extension
	auditpol.exe /set /subcategory:{0cce9211-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul
	rem System Integrity
	auditpol.exe /set /subcategory:{0cce9212-69ae-11d9-bed3-505054503030} /failure:enable /success:enable >nul 2>nul

	rem Global Object Access Auditing
	rem File System
	auditpol /resourceSACL /set /type:File /user:Everyone /success /failure /access:FRFW >nul 2>nul
	rem Registry
	auditpol /resourceSACL /set /type:Key /user:Everyone /success /failure /access:KRKW >nul 2>nul

	rem Force audit policy subcategory settings
	reg add HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Lsa\ /v scenoapplylegacyauditpolicy /t REG_DWORD /d 0x00000001 /f >nul 2>nul

	rem Back up current policy
	copy /Y "%systemroot%\system32\grouppolicy\machine\microsoft\windows nt\audit\audit.csv" audit_backup.csv >nul 2>nul

	rem Apply new policy
	if  exist audit_export.csv erase audit_export.csv /q
	auditpol /backup /file:audit_export.csv >nul 2>nul
	copy /Y audit_export.csv "%systemroot%\system32\grouppolicy\machine\microsoft\windows nt\audit\audit.csv" >nul 2>nul
	
	echo Advanced auditing configured
) else (
	echo Configuring Basic Audit Policy...
	
	if  exist sec_policy_export.inf erase sec_policy_export.inf
	secedit /export /cfg sec_policy_export.inf /quiet
	
	if  exist basic_audit.inf erase basic_audit.inf /q
	if  exist basic_audit.sdb erase basic_audit.sdb /q
	if  exist basic_audit.log erase basic_audit.log /q

	echo.[version]					>>basic_audit.inf
	echo.signature=”$CHICAGO$”		>>basic_audit.inf
	echo.[Event Audit]				>>basic_audit.inf

	echo.AuditSystemEvents = 3		>>basic_audit.inf
	echo.AuditLogonEvents = 3		>>basic_audit.inf
	echo.AuditObjectAccess = 3		>>basic_audit.inf
	echo.AuditPrivilegeUse = 3		>>basic_audit.inf
	echo.AuditPolicyChange = 3		>>basic_audit.inf
	echo.AuditAccountManage = 3		>>basic_audit.inf
	echo.AuditProcessTracking = 3	>>basic_audit.inf
	echo.AuditDSAccess = 3			>>basic_audit.inf
	echo.AuditAccountLogon = 3		>>basic_audit.inf

	secedit /configure /db basic_audit.sdb /cfg basic_audit.inf /log basic_audit.log /quiet
	echo Basic auditing configured
)