# List
List of nxlog package signing keys:
* previous/nxlog-pubkey-20200219-20210410.asc - 9354D2051DA9E40E - valid for packages published from Feb 19, 2020 to Apr 10, 2021 (nxlog-ee4 < v4.7, nxlog-ee5 < v5.3)
* previous/nxlog-pubkey-20210410-20241009.asc - 67783185632CF6DB - valid for packages published from Apr 10, 2021 to Oct 09, 2024 (nxlog-ee4 > v4.7, nxlog-ee5 v5.3 - v5.11, nxlog-ee6 v6.0 - v6.4)
* nxlog-pubkey.asc - 8255050E24C3F75D - valid for packages published starting from Oct 09, 2024 (nxlog-ee5 >=5.11, nxlog-ee6 >=6.4)
