echo "This script will download the components for process governor"
echo "WARNING: Run this script as an Administrator or it will fail"
echo "Setup the paths to where the files will be installed, press enter to leave the default paths"
$ProcGov = Read-Host "Path for Process Governor (Default: C:\Program Files\procgov)"
$NSSM = Read-Host "Path for NSSM (Default: C:\Program Files\nssm-2.24)"
if ($ProvGov -like "") {
    $ProcGov = "C:\Program Files"
}

if (Test-Path -Path $ProcGov"\procgov") {
    echo "Folder exists for Process Governor, skipping step."
} else {
    echo "Downloading Process Governor 2.9-1 zip file"
    Invoke-WebRequest -Uri "https://github.com/lowleveldesign/process-governor/archive/refs/tags/2.9-1.zip" -OutFile $env:TEMP"\procgov-2.9-1.zip"
    Expand-Archive -LiteralPath $env:TEMP"\procgov-2.9-1.zip" -DestinationPath $ProcGov
    Rename-Item $ProcGov"\process-governor-2.9-1" $ProcGov"\procgov"
}

if ($NSSM -like "") {
    $NSSM = "C:\Program Files"
}

if (Test-Path -Path $NSSM"\nssm-2.24") {
    echo "Folder exists for NSSM 2.24"
} else {
    echo "Downloading NSSM 2.24 zip file"
    Invoke-WebRequest -Uri "https://nssm.cc/release/nssm-2.24.zip" -OutFile $env:TEMP"\nssm-2.24.zip"
    Expand-Archive -LiteralPath $env:TEMP"\nssm-2.24.zip" -DestinationPath $NSSM
}

echo "Adding processor.exe to Process Governor folder"
if (Test-Path -Path $ProcGov"\procgov\processor") {
    echo "Processor folder already exists, proceeding with installation"
} else {
    New-Item -Path $ProcGov"\procgov" -Name "processor" -ItemType "directory"
}

# If the files exists then no copy will be done, otherwise proceed with copying
$exclude = Get-ChildItem -Recurse $ProcGov\procgov\processor\
Copy-Item .\dist\* -Destination $ProcGov\procgov\processor -Recurse -Exclude $exclude

echo "Creating JSON file"
@{workingdir=$ProcGov+"\procgov";logfile_folder=$ProcGov+"\procgov\processor";log_to_file="True";maxmem="200M";cpu_cores="1";cpurate="20";timer="5"} | ConvertTo-Json | Out-File -FilePath "C:\ProgramData\procgov.json"

echo "Attempting to create Windows service"
$NSSMPATH = $NSSM+"\nssm-2.24\win64\nssm.exe"
& $NSSMPath install procgov $ProcGov\procgov\processor\processor.exe
& $NSSMPath set procgov DisplayName Process Governor for NXLog
& $NSSMPath set procgov Description Process Governor middleware for NXLog
& $NSSMPath set procgov Start SERVICE_AUTO_START

# Pending automated configuration for NSSM
echo "Finished setup, process can be found as procgov under the current Windows Services"