import os
import sys
import time
import json
import getopt
# required pypiwin32 for this, win32serviceutil comes from this particular package
import win32serviceutil
import wmi
import subprocess
import logging

argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv, "h:c:")
    for opt, arg in opts:
        if opt in ['-h']:
            print("Usage: processor.exe -c <configuragion_file_path>")
        elif opt in ['-c']:
            conf_path = arg
except Exception as e:
    print(e)

try:
    try:
        if conf_path:
            pass
    except:
        conf_path = "C:\ProgramData\procgov.json"
    # This is hardcoded and could be changed to allow for someone to send an argument
    # to the service which indicates where the configuration file is
    with open(conf_path, encoding='utf-16') as json_file:
        configuration = json.load(json_file)
    log_file = configuration['logfile_folder']+"\processor.log"
    logging.basicConfig(filename=log_file, filemode='a',
                        format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)
    logging.info("Loaded configuration file successfully")
except Exception as e:
    print(e)
    logging.error(e)
    print("Exiting program")
    sys.exit()

def win32_search(proc_name):
    conn = wmi.WMI()
    for process in conn.Win32_Process():
        if proc_name in process.Name:
            return [proc_name,process.ProcessId]
        else:
            pass

def win32_start(proc_name, proc_pid, configuration):
    if proc_name == "nxlog":
        win32serviceutil.RestartService("nxlog")
        result = "Attempted to start NXLog Service..."
        print(result)
        logging.info(result)
        nxlog_info = win32_search(proc_name)
    elif proc_name == "procgov":
        os.chdir(configuration['workingdir'])
        run_procgov = '\\procgov64.exe --maxmem {} --cpu {} --cpurate {} --pid {}'.format(
            configuration['maxmem'],
            configuration['cpu_cores'],
            configuration['cpurate'],
            proc_pid)
        run_processor = run_procgov.split(" ")
        run_processor[0] = configuration['workingdir'] + run_processor[0]
        try:
            subprocess.Popen(run_processor, shell=True)
            logging.info("Started ProcessGovernor")
        except Exception as e:
            print(e)
            logging.error(e)
    else:
        pass


def write_log(content):
    if type(content) == list:
        result = "{} running [PID: {}]".format(content[0], content[1])
    elif type(content) == str:
        result = content
    elif type(content) == NoneType:
        result = "Content is empty, you may not have enough permissions to run this program."
    print(result)
    logging.info(result)

# This loop is intended to work in 60 second intervals, it can be lowered
# or increased on the configuration file as needed. Note that the timer is
# set in seconds for this reason
if __name__ == '__main__':
    try:
        while True:
            nxlog_info = win32_search("nxlog")
            if not nxlog_info:
                result = "NXLog process not found, attempting to start."
                write_log(result)
                win32_start('nxlog', None, configuration)
                nxlog_info = win32_search("nxlog")

            write_log(nxlog_info)
            procgov_info = win32_search("procgov")
            if not procgov_info:
                print("Starting Process Governor.")
                procgov_info = win32_start("procgov", nxlog_info[1], configuration)
                procgov_info = win32_search("procgov")
            write_log(procgov_info)
            result = "Checking back on processes in {} seconds".format(configuration['timer'])
            write_log(result)
            time.sleep(int(configuration['timer']))
    except KeyboardInterrupt:
        result = "Ending monitoring and Process Governor"
        write_log(result)
        conn = wmi.WMI()
        for process in conn.Win32_Process():
            if "procgov" in process.Name:
                try:
                    process.Terminate()
                except:
                    pass
                    result = "Found and terminated Process Governor"
                    write_log(result)
        result = "Exiting processor"
        write_log(result)