Python script for masking sensitive data in event log records. It can be used with the NXLog [xm_python](https://nxlog.co/documentation/nxlog-user-guide/xm_python.html) extension module.

# Script dependencies
## Python dependencies
This script uses the [passlib](https://passlib.readthedocs.io/en/stable/) library for Python. To install it using pip:

```
python -m pip install passlib
```
Make sure to install the package for your Python 3 installation. Run `python --version` to check the version of Python. On some platforms, such as Debian, you may need to reference `python3`. 

Note that the library needs to be accessible by the user running NXLog. It may be convenient to run the installation in `sudo` mode.

## NXLog modules
_xm_python_ is included in the installation package of the [supported platforms](https://nxlog.co/documentation/nxlog-user-guide/available-modules.html#modules_by_package_xm_python) and needs to be installed separately.

# Provided hashing functions
`convert_host()`
Hashes the value of the `$Hostname` field if it's available.

`ipv4_encoding()`
Searches the `$Message` field for IPv4 patterns and hashes any matching values.

`pass_lib_encoding()`
Searches the `$Message` field for a matching string and if found, processes the field further for a matching pattern. The current implementation searches for the _cc_ string and if found, continues to search for and hash MasterCard debit or credit card numbers.

`hash_lib_encoding()`
Alternative implementation of `pass_lib_encoding()` using the native [hashlib](https://docs.python.org/3/library/hashlib.html) Python module.

# NXLog configuration examples
See the NXLog documentation on [Data masking](https://nxlog.co/documentation/nxlog-user-guide/data-masking.html).

This script is provided "AS IS" without warranty of any kind, either expressed or implied. Use at your own risk.