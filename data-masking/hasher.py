import nxlog
import re
import base64
import hashlib
import uuid
from passlib.hash import pbkdf2_sha256

def regex_convert(content, controller):
    if controller == "cc":
        result = re.findall(r"(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}", content)
    elif controller == "ip":
        result = re.findall(r"(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)", content)
    elif controller == "email":
        result = re.findall(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", content)
        # In case the above does not work the regex below is 99.99% guaranteed
        # to find an e-mail address. This is explained here: https://emailregex.com/
        # result = re.search("(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",email)
    return result

def convert_host(event):
    module = event.module
    if 'Hostname' in event.field_names():
        host = event.get_field('Hostname')
        event.set_field('Hostname', pbkdf2_sha256.hash(host))

def ipv4_encoding(event):
    module = event.module
    if 'Message' in event.field_names():
        message = event.get_field('Message')
        check_ip = regex_convert(message, "ip")
        if bool(check_ip) == True:
            for ip_value in check_ip:
                hashed_field = pbkdf2_sha256.hash(ip_value)
                message = message.replace(ip_value, hashed_field)
            event.set_field('Message', message)

def pass_lib_encoding(event):
    module = event.module
    if 'Message' in event.field_names():
        message = event.get_field('Message')
        if 'cc' in message:
            check_result = regex_convert(message, "cc")
            if bool(check_result) == True:
                for cc_value in check_result:
                    hashed_field = pbkdf2_sha256.hash(cc_value)
                    message = message.replace(cc_value, hashed_field)
                event.set_field('Message', message)

# Alternative method of hashing data in messages captured by NXLog
# through a native library
def hash_lib_encoding(event):
    module = event.module
    if 'Message' in event.field_names():
        message = event.get_field('Message')
        if 'cc' in message:
            check_result = regex_convert(message, "cc")
            if bool(check_result) == True:
                for cc_value in check_result:
                    salt = uuid.uuid4().hex
                    encoded_result = cc_value.encode('ascii')
                    base64_bytes = base64.b64encode(encoded_result)
                    base64_message = base64_bytes.decode('ascii')
                    sha256_message = hashlib.sha256(base64_bytes).hexdigest()
                    salted_sha256 = hashlib.sha256((salt + base64_message).encode('ascii')).hexdigest()
                    message = message.replace(cc_value, salted_sha256)
                event.set_field('Message', message)
