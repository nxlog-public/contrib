# coding=utf-8
import requests
import argparse


class HTTPLogSender:
    def __init__(self):
        arg_obj = argparse.ArgumentParser()
        arg_obj.add_argument("--client_cert",
                             default="keys/client-cert.pem",
                             required=False,
                             dest="client_cert",
                             help="Client certificate file, full path")

        arg_obj.add_argument("--client_key",
                             default="keys/client-key.pem",
                             required=False,
                             dest="client_key",
                             help="Client certificate key file, full path")

        arg_obj.add_argument("--ca_cert",
                             default="keys/ca.pem",
                             required=False,
                             dest="ca_cert",
                             help="CA file, full path")

        arg_obj.add_argument("--url",
                             required=True,
                             dest="url",
                             help="URL to use : https://<HOSTNAME>:<PORT>")

        arg_obj.add_argument("--content_type",
                             default="text/plain",
                             required=False,
                             dest="ctype",
                             help="http content type: text/plain for basic messages")

        parsed_args = arg_obj.parse_args()
        self.url = parsed_args.url
        self.client_cert = parsed_args.client_cert
        self.client_key = parsed_args.client_key
        self.ca_cert = parsed_args.ca_cert
        self.content_type = parsed_args.ctype
        self.headers = {"Content-Type": self.content_type}

    def send_log(self, log_line):
        try:
            r = requests.post(self.url, headers=self.headers, data=log_line,
                              cert=(self.client_cert, self.client_key), verify=self.ca_cert)
            return r.status_code, r.content
        except requests.exceptions.RequestException as e:
            print("ERROR", e)

    def send_logs(self, log_lines):
        try:
            with requests.Session() as s:
                s.headers.update(self.headers)
                s.cert = (self.client_cert, self.client_key)
                s.verify = self.ca_cert
                s.stream = True

                if type(log_lines) is list:
                    for line in log_lines:
                        print("Sent data : {}".format(line))
                        s.post(self.url, data=line)
                else:
                    print("Sent data : {}".format(log_lines))
                    s.post(self.url, data=log_lines)
                s.close()
                
        except requests.exceptions.RequestException as e:
            print("ERROR", e)


sender = HTTPLogSender()

# Send a single log line
simple_event = "This is a test event"
# # using session and STREAM TRUE
sender.send_logs(simple_event)
# # without session
print(sender.send_log(simple_event))

# Send a list of log lines
my_log_lines = ["Test event 1",
                "Test event 2",
                "Test event 3"]
sender.send_logs(my_log_lines)

# Send multiline in single request body
multiline = "Test event packet \n" * 10
sender.send_logs(multiline)
