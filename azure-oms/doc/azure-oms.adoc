:addon-name: azure-oms

[id="addon-{addon-name}"]
[desc="Send logs to Azure Cloud OMS Log Analytics via the REST API"]
= Azure OMS

include::../../_asciidoctor/public.adoc[]

The Azure OMS add-on supports connecting to the Microsoft Azure Cloud
Operational Management Suite (OMS) Log Analytics system via its REST API and
sending or receiving log data. See the
link:https://docs.microsoft.com/en-us/azure/operations-management-suite/operations-management-suite-overview[Azure
OMS] and
link:https://docs.microsoft.com/en-us/azure/log-analytics/log-analytics-overview[Log
Analytics] documentation for more information about configuring and using
Azure OMS and its log management service.

== Forwarding Data to Log Analytics

The `oms-pipe.py` script performs REST API calls to send log data to the Log
Analytics service. To configure NXLog, complete the following steps.

. Log in to the Azure portal and go to the *Log Analytics* service (for
  instance by typing the service name into the search bar).

. Select an existing *OMS Workspace* or create a new one by clicking the *Add*
  button.

. From the *Management* section in the main workspace screen, click *OMS
  Portal*.
+
image::azure-oms_1.png["Shipping data to Log Analytics, screen 1", pdfwidth=604px]

. In the *Microsoft Operations Management Suite*, click the settings icon in
  the top right corner, navigate to *Settings > Connected Sources > Linux
  Servers*, and copy the *WORKSPACE ID* and *PRIMARY KEY* values. These are
  needed for API access.
+
image::azure-oms_2.png["Shipping data to Log Analytics, screen 2", pdfwidth=512px]

. Enable *Custom Logs*. As of this writing it is a preview feature, available
  under *Settings > Preview Features > Custom Logs*.
+
image::azure-oms_3.png["Shipping data to Log Analytics, screen 3", pdfwidth=511px]

. Place the `oms-pipe.py` script in a location accessible by NXLog and make
  sure it is executable by NXLog.

. Set the customer ID, shared key, and log type values in the script.

. Configure NXLog to execute the script with the <<om_exec,om_exec>>
  module. The contents of the `$raw_event` field will be forwarded.

.Sending Raw Syslog Events
====
This configuration reads raw events from file and forwards them to Azure OMS.

.nxlog.conf
[source,config]
----
include::snippets/oms-pipe-raw-syslog.conf[tag=doc_include]
----
====

.Sending JSON Log Data
====
With this configuration, NXLog Enterprise Edition reads W3C records with from
file with <<im_file,im_file>>, parses the records with <<xm_w3c,xm_w3c>>,
converts the internal event fields to JSON format with _xm_json_
<<xm_json_proc_to_json,to_json()>>, and forwards the result to Azure OMS with
<<om_exec,om_exec>>.

.nxlog.conf
[source,config]
----
include::snippets/oms-pipe-json.conf[tag=doc_include]
----
====

== Downloading Data From Log Analytics

The `oms-download.py` Python script implements the OMS API and performs a REST
API call for downloading data from Log Analytics. To set it up with NXLog,
follow these steps:

. Register an application in *Azure Active Directory* and generate an access
  key for the application.

. Under your *Subscription*, go to *Access control (IAM)* and assign the *Log
  Analytics Reader* role to this application.

. Place the `oms-download.py` script in a location accessible by NXLog.

. Set the resource group, workspace, subscription ID, tenant ID, application
  ID, and application key values in the script. Adjust the query details as
  required.
+
NOTE: The Tenant ID can be found as *Directory ID* under the Azure Active
      Directory *Properties* tab.

. Configure NXLog to execute the script with the <<im_python,im_python>>
  module.

Detailed instructions on this topic can be found in the
link:https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal[Azure
documentation].

.Collecting Logs From OMS
====
This configuration uses the <<im_python,im_python>> module and the
`oms-download.py` script to periodically collect log data from the Log
Analytics service.

.nxlog.conf
[source,config]
----
include::snippets/oms-download.conf[tag=doc_include]
----
====
